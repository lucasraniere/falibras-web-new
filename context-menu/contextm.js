// criando o menu de contexto
browser.contextMenus.create({
	id: "falibras-web",
	title: "Traduzir com FalibrasWeb",
	contexts: ["selection"]
});

// adicionando acao ao menu de contexto
browser.contextMenus.onClicked.addListener((info, tab) => {

	js = "{\"frase\":\"" + info.selectionText	+  "\"}"
	console.log(js);

	browser.windows.create({
		url: "popup/popup.html",
		type: "popup",
		height: 400,
		width: 340
	})
});
