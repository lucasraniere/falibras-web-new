main();


function main() {
    "use strict";
    
    var canvas = document.createElement('canvas');
    
    canvas.setAttribute('id','glCanvas');
    
    document.getElementsByClassName('content1').appendChild(canvas);
    
    var canvas1 = document.querySelector('#glCanvas');
    var gl = canvas.getContext("webgl");

    if (gl === null) {
        alert("Incapaz de inicializar o WebGL.")
        return;
    }

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);    
}