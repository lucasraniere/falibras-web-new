function isNumber(n) {
  return !isNaN(n);
  //return !isNaN(parseFloat(n)) && isFinite(n);
}


function pegarInformacoes(){

  var nome = formulario.nomeCompleto.value;
  var idade = formulario.idade.value;
  var cep = formulario.cep.value;
  var endereco = formulario.endereco.value;
  var bairro = formulario.bairro.value;

  var confIdade = isNumber(idade);

  if (nome == ""){
    formulario.resultado.value = "Digite seu nome!";
  }
  else if (idade == ""){
    formulario.resultado.value = "Digite sua idade!"
  }
  else if (confIdade) {
    formulario.resultado.value =  "Nome " + nome + "\nIdade "+  idade + "\nCEP " + cep + "\nEndereço " + endereco + "\nBairro " + bairro;
  } else if (confIdade == false){
    formulario.resultado.value = "Campo Idade deve ser um número inteiro!"
  }
}
